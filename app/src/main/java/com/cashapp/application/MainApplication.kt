package com.cashapp.application

import android.app.Application
import android.content.Context
import android.content.res.Configuration

class MainApplication : Application() {
    private var mainApplication: MainApplication? = null

    override fun onCreate() {
        super.onCreate()
        this.mainApplication = this
    }
    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }

    companion object {
        @get:Synchronized
        var instance: MainApplication? = null
            private set
    }
}