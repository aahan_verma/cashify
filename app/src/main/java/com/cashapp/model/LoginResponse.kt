package com.cashapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("error")
    @Expose
    var error: String? = null

    @SerializedName("mobile")
    @Expose
    var mobile: String? = null

    @SerializedName("id")
    @Expose
    var id: String? = null
}