package com.cashapp.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GenricModel {
    @SerializedName("mobile")
    @Expose
    var mobile: String? = null

    @SerializedName("otp")
    @Expose
    var otp: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("refercode")
    @Expose
    var refercode: String? = null
}