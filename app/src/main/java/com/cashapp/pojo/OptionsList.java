package com.cashapp.pojo;

public class OptionsList {
    String name;
    int resID;

    public OptionsList(String name, int resID) {
        this.name = name;
        this.resID = resID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResID() {
        return resID;
    }

    public void setResID(int resID) {
        this.resID = resID;
    }
}
