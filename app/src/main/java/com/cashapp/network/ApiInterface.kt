package com.cashapp.network


import com.cashapp.model.LoginResponse
import com.cashapp.model.OtpResponse
import com.cashapp.pojo.GenricModel
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @POST("users/applogin")
    fun doLogin(
        @Body obj : GenricModel
    ): Call<LoginResponse?>?

    @POST("users/checkotp")
    fun callOtp(
        @Body obj :GenricModel
    ): Call<OtpResponse?>?

    @POST("users/basicprofile")
    fun resgisterUser(
        @Header("Authorization") auth :String,
        @Body obj :GenricModel
    ): Call<OtpResponse?>?

}