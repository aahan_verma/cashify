package com.boigo.app.retrofit


import com.cashapp.network.ApiInterface
import com.cashapp.utils.UtilsString.Companion.BASE_URL
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RetrofitHandler {
    private var apiInterface: ApiInterface? = null
    private fun ApiClient() {
        try {
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addNetworkInterceptor(header)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build()
            apiInterface = retrofit.create(ApiInterface::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    val api: ApiInterface?
        get() {
            if (apiInterface == null) {
                uniqInstance!!.ApiClient()
            }
            return apiInterface
        }

    var header = Interceptor { chain: Interceptor.Chain ->
        val builder = chain.request().newBuilder()
        builder.addHeader("device-type", "Android")
        builder.addHeader("app-version", "1.0")
        builder.addHeader("Content-Type", "application/json")
        chain.proceed(builder.build())
    }

    companion object {
        private var uniqInstance: RetrofitHandler? = null
        @get:Synchronized
        val instance: RetrofitHandler?
            get() {
                if (uniqInstance == null) {
                    uniqInstance = RetrofitHandler()
                }
                return uniqInstance
            }
    }
}