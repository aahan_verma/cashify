package com.cashapp.appinteface

interface NetworkCheck {
    fun internetStatus(connection: Boolean)
}