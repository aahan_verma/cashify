package com.cashapp.ui.activity

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import com.cashapp.R
import com.cashapp.base.BaseActivity
import com.cashapp.presenter.home.HomeFragment
import com.cashapp.presenter.login.LoginFragment
import com.cashapp.presenter.otp.OtpFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (intent.extras == null) {
            replaceFragment(LoginFragment(),R.id.fl_main,false,false)
        }
    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            window.statusBarColor = resources.getColor(R.color.app_main_color)
        }
    }

}
