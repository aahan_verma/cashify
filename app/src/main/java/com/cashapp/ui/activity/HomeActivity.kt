package com.cashapp.ui.activity

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import com.cashapp.R
import com.cashapp.base.BaseActivity
import com.cashapp.presenter.home.HomeFragment
import com.cashapp.presenter.profile.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private var doubleBackToExitPressedOnce = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        replaceFragment(HomeFragment(), R.id.fl_home, false, false)

        tabLayout.setOnNavigationItemSelectedListener(this)
    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            window.statusBarColor = resources.getColor(android.R.color.transparent)
        }
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fl_home)
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            doBackPressStuff()
        }
    }

    private fun doBackPressStuff() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.m_home -> {
                addfragmentwithSingleAnim(HomeFragment(), R.id.fl_home, false, true)
                return true
            }
            R.id.m_order -> {

                return true
            }
            R.id.m_help -> {

                return true
            }
            R.id.m_profile -> {
                addfragmentwithSingleAnim(ProfileFragment(), R.id.fl_home, false, true)
                return true
            }
        }
        return false
    }


}