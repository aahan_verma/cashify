package com.cashapp.ui.activity

import android.os.Bundle
import com.cashapp.R
import com.cashapp.base.BaseActivity
import com.cashapp.utils.UtilsString.Companion.SPLASH_TIME_OUT
import com.cashapp.utils.prefstore.SharedPrefsManager

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


    }

    override fun onPause() {
        super.onPause()
        if (runobj != null) {
            mHandler.removeCallbacks(runobj!!)
        }
    }

    override fun onResume() {
        super.onResume()
        runobj = Runnable {
            if(SharedPrefsManager.getIsUserLoggedIn(this@SplashActivity)) {
               goToHomeActivity()
            }else{
                goToMainActivity()
            }
        }
        mHandler.postDelayed(runobj!!, SPLASH_TIME_OUT.toLong())
    }
}