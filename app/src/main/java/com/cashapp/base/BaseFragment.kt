package com.cashapp.base

import android.content.Context
import androidx.fragment.app.Fragment

open class BaseFragment :Fragment() {

    var rootActivity : BaseActivity?= null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.rootActivity = context as BaseActivity
    }
}