package com.cashapp.base

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Patterns
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.cashapp.R
import com.cashapp.ui.activity.HomeActivity
import com.cashapp.ui.activity.MainActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.util.*

open class BaseActivity :AppCompatActivity() {

    private var progress: Dialog? =null

    /**
     * Permissions to be accessed/requested to user
     **/
    private val PERMISSIONARRAY = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    /**
     * Delay Handlers
     * */
    val mHandler = Handler()
    var runobj: Runnable? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /**
         * For Making full Screen
         **/
        if (Build.VERSION.SDK_INT in 19..20) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true)
        }
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val decor: View = window.decorView
            decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }

    }

    fun setWindowFlag(activity: BaseActivity, bits: Int, on: Boolean) {
        val win = activity.window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }

    fun askBasicPermission() {
        Dexter.withActivity(this)
            .withPermissions(PERMISSIONARRAY[0], PERMISSIONARRAY[1], PERMISSIONARRAY[2])
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {

                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            })
            .onSameThread()
            .check()
    }



    fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage(getString(R.string.app_name) + " needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog, i ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        builder.show()
    }

    fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    fun checkEmailValid(stringEmail: String): Boolean {
        return !Patterns.EMAIL_ADDRESS.matcher(stringEmail).matches()
    }
    fun checkEmptyField(stringVal: String): Boolean {
        return stringVal == ""
    }

    fun checkIfStringIsPhone(string: String): Boolean {
        return Patterns.PHONE.matcher(string).matches() && string.length <= 9
    }
    fun showToast(message:String){
        Toast.makeText(this@BaseActivity,message,Toast.LENGTH_SHORT).show()
    }
    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            window.statusBarColor = resources.getColor(R.color.app_main_color)
        }
    }
    fun goToMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finishAffinity()
    }
    fun goToHomeActivity(){
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finishAffinity()
    }

    /**
     * This method will replace fragment in the frame
     * @fragment : Desired Fragment
     * @frame : Frame in which Fragment to be replaced
     * @addtoStack :True if we want to add to back stack or not.
     * */
    fun replaceFragment(fragment: Fragment, frame: Int, addtoStack: Boolean, allowAnim: Boolean) {
        val ft = supportFragmentManager.beginTransaction()

        if (addtoStack) {
            ft.addToBackStack(fragment.javaClass.name)
        }
        if (allowAnim) {
            ft.setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
        }
        ft.replace(frame, fragment)
        ft.commit()
    }
    fun addFragment(fragment: Fragment, frame: Int, addtoStack: Boolean, allowAnim: Boolean) {
        val ft = supportFragmentManager.beginTransaction()

        if (addtoStack) {
            ft.addToBackStack(fragment.javaClass.name)
        }
        if (allowAnim) {
            ft.setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
        }
        ft.add(frame, fragment)
        ft.commit()
    }
    fun addfragmentwithSingleAnim(fragment: Fragment, frame: Int, addtoStack: Boolean, allowAnim: Boolean) {
        val ft = supportFragmentManager.beginTransaction()

        if (addtoStack) {
            ft.addToBackStack(fragment.javaClass.name)
        }
        if (allowAnim) {
            ft.setCustomAnimations(
                R.anim.fade_in,
                R.anim.fade_out
            )
        }
        ft.replace(frame, fragment)
        ft.commit()
    }
    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        try {
            val view = currentFocus
            if (view != null &&
                (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE)
                && !view.javaClass.name.startsWith("android.webkit.")
            ) {
                val scrcoords = IntArray(2)
                view.getLocationOnScreen(scrcoords)
                val x = ev.rawX + view.left - scrcoords[0]
                val y = ev.rawY + view.top - scrcoords[1]
                if (x < view.left || x > view.right || y < view.top || y > view.bottom)
                    (Objects.requireNonNull(this.getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager)
                        .hideSoftInputFromWindow(this.window.decorView.applicationWindowToken, 0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return super.dispatchTouchEvent(ev)
    }

    fun showProgress(){
        progress = Dialog(this@BaseActivity)
        progress!!.setContentView(R.layout.progress_view)
        progress!!.setCancelable(false)
        progress!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progress!!.show()
    }
    fun hideProgress(){
        if (progress!=null && progress!!.isShowing){
            progress?.hide()
        }
    }
}