package com.linklive.app.utils.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import com.cashapp.appinteface.NetworkCheck

class NetworkChangeReceiver : BroadcastReceiver() {

    lateinit var networkCheck: NetworkCheck

    override fun onReceive(context: Context, intent: Intent) {
        try {
            if (isOnline(context)) {
                Log.e("NetworkChangeReceiver", "Online Connect Internet")
                networkCheck.internetStatus(true)
            } else {
                Log.e("NetworkChangeReceiver", "Connectivity Failure !!! ")
                networkCheck.internetStatus(false)
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    fun networkRegister(networkCheck: NetworkCheck){
        this.networkCheck = networkCheck
    }
    
    private fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw= connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }
}