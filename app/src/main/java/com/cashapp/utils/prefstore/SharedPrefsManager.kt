package com.cashapp.utils.prefstore

import android.content.Context
import androidx.preference.PreferenceManager
import com.cashapp.utils.UtilsString.Companion.IS_USER_LOGGED_IN

object SharedPrefsManager {

    fun setIsUserLoggedIn(context: Context, userloggedIn: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
            .putBoolean(IS_USER_LOGGED_IN, userloggedIn).apply()
    }

    fun getIsUserLoggedIn(context: Context): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getBoolean(IS_USER_LOGGED_IN, false)

    }
    fun setAuthToken(context: Context, Auth_Token: String) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
            .putString("Auth_Token", Auth_Token).apply()
    }

    fun getAuthToken(context: Context): String? {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getString("Auth_Token", "")

    }



    fun clearPrefs(context: Context) {
        setIsUserLoggedIn(context, false)
    }
}