package com.cashapp.utils

interface UtilsString {
    companion object {
        val IS_USER_LOGGED_IN = "IS_USER_LOGGED_IN"
        val SPLASH_TIME_OUT = 2000
        val BASE_URL = "http://34.70.182.146:3000/v1/"
    }
}
