package com.cashapp.presenter.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cashapp.R;
import com.cashapp.base.BaseActivity;
import com.cashapp.pojo.OptionsList;

import java.util.List;

public class WhatWouldLikeToDoAdapter extends RecyclerView.Adapter<WhatWouldLikeToDoAdapter.ItemViewHolder> {

    List<OptionsList> optionsLists;
    BaseActivity baseActivity;

    public WhatWouldLikeToDoAdapter(List<OptionsList> optionsLists, BaseActivity baseActivity) {
        this.optionsLists = optionsLists;
        this.baseActivity = baseActivity;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_whatyou_like_to,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        holder.iv_image.setImageResource(optionsLists.get(position).getResID());
        holder.tv_type.setText(optionsLists.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return optionsLists.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_image;
        TextView tv_type;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_type = itemView.findViewById(R.id.tv_type);
            iv_image = itemView.findViewById(R.id.iv_image);
        }
    }
}
