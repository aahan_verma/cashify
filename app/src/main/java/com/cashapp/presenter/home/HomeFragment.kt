package com.cashapp.presenter.home

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.cashapp.R
import com.cashapp.base.BaseFragment
import com.cashapp.pojo.OptionsList
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {
    var deviceAdapter :DeviceOffersAdapter? =null
    var  optionsList : ArrayList<OptionsList>? =null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home,container,false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    private fun setUpViews() {
        deviceAdapter = DeviceOffersAdapter()
        rv_device_offers.adapter = deviceAdapter

        optionsList = ArrayList()
        optionsList?.add(OptionsList("Sell Phone",R.drawable.ic_sell_phone))
        optionsList?.add(OptionsList("Repair Phone",R.drawable.ic_repair))
        optionsList?.add(OptionsList("Sell Laptop",R.drawable.ic_laptop))
        optionsList?.add(OptionsList("Buy Phone",R.drawable.ic_buy_phone))
        optionsList?.add(OptionsList("Sell Smart Watch",R.drawable.ic_watch))
        optionsList?.add(OptionsList("More",R.drawable.ic_more))

        val adapter = WhatWouldLikeToDoAdapter(optionsList,rootActivity!!)
        rv_options.adapter =adapter
    }

    override fun onResume() {
        super.onResume()
        rootActivity!!.setWindowFlag(rootActivity!!, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
        activity!!.window.statusBarColor = resources.getColor(android.R.color.transparent)
    }
}