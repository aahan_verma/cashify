package com.cashapp.presenter.otp

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.boigo.app.retrofit.RetrofitHandler
import com.cashapp.R
import com.cashapp.base.BaseFragment
import com.cashapp.model.OtpResponse
import com.cashapp.pojo.GenricModel
import com.cashapp.presenter.register.RegisterFragment
import com.cashapp.utils.prefstore.SharedPrefsManager
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_otp.*
import kotlinx.android.synthetic.main.fragment_otp.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class OtpFragment(var phone: String) : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_otp, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews(view)
        val msg = "Please enter the OTP we have sent on $phone Changer Number"
        view.tv_otp_msg.text =msg

    }

    override fun onResume() {
        super.onResume()

    }

    private fun callotp() {
        rootActivity!!.showProgress()
        var otp = "1234"
        try {
            val obj = GenricModel()
            obj.mobile = phone
            obj.otp = otp
            val logincall = RetrofitHandler.instance!!.api!!.callOtp(obj)
            logincall!!.enqueue(object : Callback<OtpResponse?> {
                override fun onFailure(call: Call<OtpResponse?>, t: Throwable) {
                    rootActivity?.hideProgress()
                    rootActivity?.showToast("Something went wring try again!!")
                }

                override fun onResponse(
                    call: Call<OtpResponse?>,
                    response: Response<OtpResponse?>
                ) {
                    if (response.isSuccessful) {
                        rootActivity!!.hideProgress()
                        if (response.body()!!.status == 1) {
                            SharedPrefsManager.setAuthToken(rootActivity!!,response.body()!!.token!!)
                            try {
                                if (response.body()?.userdata!=null
                                    && response.body()?.userdata!![0]!!.name != null){
                                    SharedPrefsManager.setIsUserLoggedIn(rootActivity!!,true)
                                    rootActivity!!.goToHomeActivity()
                                }else{
                                    rootActivity?.replaceFragment(RegisterFragment(phone), R.id.fl_main, true, true)
                                }
                            }catch (e:Exception){
                                e.printStackTrace()
                                rootActivity?.replaceFragment(RegisterFragment(phone), R.id.fl_main, true, true)
                            }

                        } else {
                            rootActivity?.showToast(response.body()!!.error!!)
                        }
                    }
                }

            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setUpViews(view: View) {
        view.et_opt_1.addTextChangedListener(GenericTextWatcher(view.et_opt_1))
        view.et_opt_2.addTextChangedListener(GenericTextWatcher(view.et_opt_2))
        view.et_opt_3.addTextChangedListener(GenericTextWatcher(view.et_opt_3))
        view.et_opt_4.addTextChangedListener(GenericTextWatcher(view.et_opt_4))

        view.tv_confirm.setOnClickListener {
            if (validate()) {
                callotp()
            } else {
                rootActivity?.showToast("Please enter valid OTP")
            }
        }
    }

    private fun validate(): Boolean {
        return et_opt_1.text.toString() != "" &&
                et_opt_2.text.toString() != "" &&
                et_opt_3.text.toString() != "" &&
                et_opt_4.text.toString() != ""
    }

    inner class GenericTextWatcher(var view: View?) : TextWatcher {

        override
        fun afterTextChanged(editable: Editable) {
            val text = editable.toString()
            when (view!!.id) {
                R.id.et_opt_1 -> if (text.length == 1) et_opt_2.requestFocus()
                R.id.et_opt_2 -> if (text.length == 1) et_opt_3.requestFocus() else if (text.isEmpty()) et_opt_1.requestFocus()
                R.id.et_opt_3 -> if (text.length == 1) et_opt_4.requestFocus() else if (text.isEmpty()) et_opt_2.requestFocus()
                R.id.et_opt_4 -> if (text.length == 0) et_opt_3.requestFocus()
            }
        }

        override
        fun beforeTextChanged(
            arg0: CharSequence?,
            arg1: Int,
            arg2: Int,
            arg3: Int
        ) { // TODO Auto-generated method stub
        }

        override
        fun onTextChanged(
            arg0: CharSequence?,
            arg1: Int,
            arg2: Int,
            arg3: Int
        ) { // TODO Auto-generated method stub
        }
    }

}
