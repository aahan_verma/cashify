package com.cashapp.presenter.login

import com.cashapp.base.BaseActivity

interface ILoginPresenter {
    fun callLogin(phone: String?, baseActivity: BaseActivity?)
    fun onDestroy()
}