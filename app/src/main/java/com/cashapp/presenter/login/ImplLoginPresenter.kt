package com.cashapp.presenter.login

import com.boigo.app.retrofit.RetrofitHandler
import com.cashapp.base.BaseActivity
import com.cashapp.model.LoginResponse
import com.cashapp.pojo.GenricModel
import com.cashapp.presenter.IViewListener
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImplLoginPresenter(var listener: IViewListener?) : ILoginPresenter {
    override fun callLogin(
        phone: String?,
        baseActivity: BaseActivity?) {
        if (listener!=null) {
            if (phone?.isEmpty()!!) {
                listener?.onFailure("Please enter valid mobile number")
            }else{
                try {
                    baseActivity?.showProgress()
                    val obj = GenricModel()
                    obj.mobile = phone
                    val logincall = RetrofitHandler.instance!!.api!!.doLogin(obj)
                    logincall!!.enqueue(object : Callback<LoginResponse?>{
                        override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                            baseActivity?.hideProgress()
                            listener?.onFailure("Something went wrong try again!")
                        }

                        override fun onResponse(
                            call: Call<LoginResponse?>,
                            response: Response<LoginResponse?>
                        ) {
                            baseActivity?.hideProgress()
                           if (response!!.isSuccessful ){
                               if (response.body()!!.status==1){
                                   listener?.onSuccess(response.body())
                               }else{
                                   listener?.onFailure(response.body()!!.error)
                               }
                           }
                        }

                    })
                }catch (e:Exception) {
                    baseActivity?.hideProgress()
                    e.printStackTrace()
                }


            }
        }
    }

    override fun onDestroy() {
        this.listener = null
    }

}