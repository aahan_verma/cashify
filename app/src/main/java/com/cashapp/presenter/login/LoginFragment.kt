package com.cashapp.presenter.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cashapp.R
import com.cashapp.base.BaseFragment
import com.cashapp.presenter.IViewListener
import com.cashapp.presenter.otp.OtpFragment
import com.cashapp.presenter.register.RegisterFragment
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.android.synthetic.main.fragment_login.view.et_l_phone

class LoginFragment : BaseFragment(), IViewListener {
    var implLoginPresenter: ImplLoginPresenter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        implLoginPresenter = ImplLoginPresenter(this)
        setupViews(view)
    }

    private fun setupViews(view: View) {
        view.tv_login.setOnClickListener {
            implLoginPresenter?.callLogin(view.et_l_phone.text.toString(),rootActivity!!)
        }

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onSuccess(obj: Any?) {
        rootActivity?.replaceFragment(OtpFragment(et_l_phone.text.toString()), R.id.fl_main, true, true)
    }

    override fun onFailure(message: String?) {
        rootActivity?.showToast(message!!)
    }
}