package com.cashapp.presenter

interface IViewListener {
    fun onSuccess(obj: Any?)
    fun onFailure(message: String?)
}