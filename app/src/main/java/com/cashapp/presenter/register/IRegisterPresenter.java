package com.cashapp.presenter.register;

import com.cashapp.base.BaseActivity;

public interface IRegisterPresenter {
    void onCallRegister(String email, String phone, String name, BaseActivity baseActivity);
    void onDestroy();
}
