package com.cashapp.presenter.register

import com.boigo.app.retrofit.RetrofitHandler
import com.cashapp.base.BaseActivity
import com.cashapp.model.OtpResponse
import com.cashapp.pojo.GenricModel
import com.cashapp.presenter.IViewListener
import com.cashapp.utils.prefstore.SharedPrefsManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImplRegisterPresenter(var listener: IViewListener?) : IRegisterPresenter {
    override fun onCallRegister(
        email: String,
        referal: String,
        name: String,
        rootActivity: BaseActivity
    ) {
        if (listener != null) {
             if (name?.isEmpty()) {
                listener?.onFailure("Please enter valid name")
            } else if (rootActivity.checkEmailValid(email)) {
                listener?.onFailure("Please enter valid email")
            } else {
              /*  listener?.onSuccess("")*/
                 try {
                     rootActivity?.showProgress()
                     val obj = GenricModel()
                     obj.name = name
                     obj.refercode = referal
                     obj.email = email
                     val logincall = RetrofitHandler.instance!!.api!!.resgisterUser(SharedPrefsManager.getAuthToken(rootActivity)!!,obj)
                     logincall!!.enqueue(object : Callback<OtpResponse?> {
                         override fun onFailure(call: Call<OtpResponse?>, t: Throwable) {
                             rootActivity?.hideProgress()
                             rootActivity?.showToast("Something went wring try again!!")
                         }

                         override fun onResponse(
                             call: Call<OtpResponse?>,
                             response: Response<OtpResponse?>
                         ) {
                             if (response.isSuccessful) {
                                 if (response.body()!!.status == 1) {
                                       listener?.onSuccess(response.body())
                                 } else {
                                     rootActivity?.showToast(response.body()!!.error!!)
                                 }
                             }
                         }

                     })
                 } catch (e: Exception) {
                     e.printStackTrace()
                 }
            }
        }
    }

    override fun onDestroy() {
        listener = null
    }

}