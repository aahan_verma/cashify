package com.cashapp.presenter.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cashapp.R
import com.cashapp.base.BaseFragment
import com.cashapp.presenter.IViewListener
import com.cashapp.utils.prefstore.SharedPrefsManager
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.fragment_register.view.*

class RegisterFragment(phone: String) : BaseFragment(), IViewListener {
    var implRegisterPresenter: ImplRegisterPresenter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        implRegisterPresenter = ImplRegisterPresenter(this)
        setupViews(view)
    }

    private fun setupViews(view: View) {
        view.cb_referal_code.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                view.til_referral.visibility = View.VISIBLE
            } else {
                view.til_referral.visibility = View.GONE
            }

        }
        view.tv_register.setOnClickListener {
            implRegisterPresenter?.onCallRegister(
                et_email.text.toString(),
                et_referral_code.text.toString(),
                et_name.text.toString(),
                rootActivity!!
            )
        }
    }

    override fun onSuccess(obj: Any?) {
        SharedPrefsManager.setIsUserLoggedIn(rootActivity!!, true)
        rootActivity?.goToHomeActivity()
    }

    override fun onFailure(message: String?) {
        rootActivity?.showToast(message!!)
    }


}